<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/brands.css" integrity="sha384-QT2Z8ljl3UupqMtQNmPyhSPO/d5qbrzWmFxJqmY7tqoTuT2YrQLEqzvVOP2cT5XW" crossorigin="anonymous">

<i class="fab fa-mastodon" ></i> [https://mastodont.cat/@apoderadigital](https://mastodont.cat/@apoderadigital)

<i class="fab fa-twitter" ></i> [https://twitter.com/ApoderaDigital](https://twitter.com/ApoderaDigital)

<i class="fab fa-facebook"></i> [https://www.facebook.com/ApoderamentDigital](https://www.facebook.com/ApoderamentDigital)

<i class="fas fa-at"></i> [info@apoderamentdigital.cat](mailto:info@apoderamentdigital.cat)
