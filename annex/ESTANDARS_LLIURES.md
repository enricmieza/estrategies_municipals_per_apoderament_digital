## Propostes referents a estàndards lliures ##

Els documents, dades i interacció entre els serveis municipals i els ciutadans son una necessitat i un recurs per a la ciutadania per molts aspectes de la seva vida. El format i les eines d'accés no han de ser mai obstacle.
Les especificacions i formats lliures estan totalment documentats i estan disponible públicament; han de ser lliures de restriccions de drets d'autor, reclamacions de propietat intel·lectual o llicències restrictives; el format es decideix per una organització de normes independent d'un proveïdor o per a una comunitat (p. ex., comunitat de desenvolupament de codi obert); hi han implementacions de eines de suport de lliure us.

**Objectius**
 * Facilitar la interoperabilitat i compartició de les dades en els serveis finançats amb fons públics pel benefici dels ciutadans, evitant barreres d'accés, fent servir especificacions per formats de dades i serveis que segueixin estàndards oberts.
 * Evitar crear dependències i costos de tercers: preferència per estàndards de formats de dades i protocols lliures de patents, llicències, drets intel·lectuals.
 * Evitar condicionar decisions amb la preferència per protocols lliures, àmpliament implementats i aplicats en diversitat de productes i serveis.

**Proposta programàtica**
 * Assegurar la interoperativitat: utilitzar formats oberts de fitxers com a eina per garantir l'accés a la informació sense obstacles i la bona interacció amb i entre els sistemes públics. A més a més, els estàndards que s'utilitzin han de tenir implementacions obertes que es verifiqui funcionin amb una varietat de dispositius o programari, inclouent sempre una opció de programari lliure.
 * Un dels requisits alhora de comprar aparells tecnològics ha de ser que aquests utilitzin estàndards lliures per realitzar la seva funció. D'aquesta manera s'eliminen les dependències cap a un sol fabricant.
 * Producció digital utilitzant llicències copyleft i Creative Commons que facilitin la reutilització de les obres artístiques i produccions dels/de les creadors/es locals finançades amb fons públics, com a eines de cultura lliure.

**Experiències inspiradores**
 * Compliment amb estàndards de W3C. Ha aconseguit que puguem accedir a qualsevol pàgina web des de qualsevol sistema sense haver d'anar en compte amb la plataforma que executa l'usuari. https://w3c.org
 * Preferència per formats lliures amb eines lliures per creació i edició: format ODF (OpenDocument Format) http://en.wikipedia.org/wiki/OpenDocument com a estàndard enlloc de Microsoft Office Open XML http://en.wikipedia.org/wiki/Office_Open_XML
 * Estàndards d'Internet de l'IETF.org que permeten la interconnexió de xarxes que és Internet i les aplicacions segures com HTTPS. Estàndars de xarxes d'accés com IEEE que fa posible l'accés universal a xarxes WiFi municipals.
 * Preferència pels formats més lliures per l'arxiu i la preservació de dades: https://suchanek.name/texts/archiving/index.html
