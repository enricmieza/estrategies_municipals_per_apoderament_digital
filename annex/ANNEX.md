# Annex

En aquest apartat hi ha una explicació més extensa de cada proposta, experiències concretes, bibliografia, i consells d'implementació.
Està dividit en els següents apartats:

* [Infraestructura de telecomunicacions](./INFRAESTRUCTURA_DE_TELECOMUNICACIONS.md)
* [Programari Lliure](./PROGRAMARI_LLIURE.md)
* [Estàndards lliures](./ESTANDARS_LLIURES.md)
* [Política de dades](./POLÍTICA_DE_DADES.md)
* [Democratització de la tecnologia](./DEMOCRATITZACIÓ_DE_LA_TECNOLOGIA.md)
* [Compra pública de dispositius electrònics i circularitat](./COMPRA_PUBLICA_DE_DISPOSITIUS_ELECTRONICS_I_CIRCULARITAT.md)