## Medidas para el empoderamiento digital en los municipios

### Infraestructura de telecomunicaciones

La infraestructura de telecomunicaciones es todo aquel cableado y hardware que hace posible que la información viaje de un lugar a otro.
Es el pilar de Internet.
Actualmente tenemos infraestructuras públicas, privadas y procomunes, dependiendo de quién sea su propietario y qué modelo de gestión tenga.

Las decisiones que se toman en relación a la regulación, evolución o ampliación de esta infraestructura han sido objeto de fuertes tensiones entre diferentes intereses privados que buscan controlar la red y la información que circula a través de ella para poder sacar un beneficio.
También hemos podido ver como el control de esta red se ha utilizado dentro de campañas de censura.


#### Objetivos generales

* Tender hacia la universalización y soberanía de las redes de
  telecomunicaciones, potenciando el uso neutral de las redes de
  telecomunicaciones.

* Impulsar que el despliegue de servicios de telecomunicaciones fijos y
  móviles, existentes y nuevos como el 5G, se realice a través de una red
  compartida por los distintos operadores y usuarios públicos y privados,
  evitando la privatización del espacio público sin compartición.

* Potenciar las operadoras de proximidad y de carácter cooperativo, evitando
  el monopolio de las grandes operadoras, incrementando su control.

* Desplegar y consolidar una red de datos de Internet de las cosas abierta,
  libre y neutral, creada colectivamente desde abajo.


#### Propuesta programática

##### Consolidación de un ecosistema de infrastructura colaborativa y abierta con tecnologías libres

1. Proporcionar un acceso real para los ciudadanos y la sociedad en general
   a una oferta asequible y variada de servicios de telecomunicaciones de la
   máxima calidad, capacidad y neutralidad.

2. Promover ordenanzas municipales de despliegue de nuevas infraestructuras
   como la propuesta por guifi.net, que promueve con la máxima agilidad y
   eficiencia posible, estimula y maximiza la eficiencia de cualquier tipo
   de inversión y, al mismo tiempo, asegura la sostenibilidad en función del
   uso realizado, minimizando el coste para la administración pública y
   también para el ciudadano y la sociedad en general.

3. Alentar las redes abiertas y comunitarias, incidiendo en la necesidad de
   evaluar el impacto social y territorial de los despliegues, teniendo en
   cuenta los posibles casos de uso.

##### Despliegue de nuevas infraestructuras para servicios de comunicaciones (existentes o de nueva generación como el 5G y futuros)

4. Promover el despliegue de infraestructuras de acceso (fijas y móviles,
   nuevas o existentes como 5G) a través de una red compartida por
   diferentes operadores.

5. Facilitar el intercambio de infraestructura pública: implementación de
   antenas o paso de cableado en edificios e infraestructuras públicas.

##### Contratación de servicios de telecomunicaciones

6. Contratar servicios a empresas de telecomunicaciones que no discriminen
   el tráfico, lo filtren o interrumpan, como en el caso del referéndum de
   autodeterminación del 1 de octubre de 2017.

##### La Internet de las cosas

7. Promover iniciativas de redes de Internet de las cosas abiertas, libres y
   neutrales.

8. Contribuir a una arquitectura de Internet de las cosas para las ciudades
   que sea abierta e interoperable.


### Software libre

Cuando un programa informático se puede utilizar, modificar, compartir y estudiar, lo denominamos libre.
El software libre nos permite entonces resolver las necesidades informáticas comunes de una manera inclusiva, en la que diferentes actores pueden tener un interés proactivo en una solución sin tener que desarrollarla dos veces o pedirle a un tercero que lo haga para nosotros.
Evitar la dependencia de terceros que tienen un control sobre el programa y hace posible verificar que es exactamente lo que hace el programa, tanto para asegurarse de que hace lo que debe hacer, como para asegurarse que no está tratando de nuestros datos de formas no deseadas.

#### Objetivos generales

* Alcanzar la plena soberanía municipal respecto a qué código y algoritmos se ejecutan, qué datos se almacenan y quién tiene acceso.
Haga que el código auditable por la ciudadanía.

* Independizar a los ayuntamientos de las estrategias comerciales de los
  proveedores, mejorando la capacidad de decisión sobre qué tecnologías se
  implantan al mismo tiempo que promueve la innovación y el fortalecimiento
  del tejido tecnológico local.

* Obtener las soluciones informáticas mejor adaptadas a cada caso y más
  fáciles de evolucionar, a un menor coste.

* Contribuir a hacer sostenible el patrimonio digital que supone el software
  libre.

#### Propuesta programática

##### Relación con la ciudadanía

9. Garantizar que todas las webs y aplicaciones municipales funcionen sin
   ninguna desventaja con software libre.

10. Utilizar únicamente formatos y protocolos abiertos, con especificaciones
    públicas y que dispongan de buenas implementaciones libres.

11. No incorporar a las webs municipales servicios de terceros basados en software privativo, o que permitan la recolección de datos por parte de quien presta el servicio.
En general, no distribuir software privativo de ninguna forma.

12. No aceptar donaciones de software privativo por parte de empresas privadas, ni de hardware que requiera el uso de software privativo para un correcto funcionamiento.
Este punto es particularmente importante en el campo de la educación.

13. Ofrecer software que funcione con la lengua propia de la
    ciudadanía. Sistemas traducidos incluyendo mensajes de error,
    documentación, diccionarios y sistemas de habla.

##### Implementación de software libre en cada municipio

14. Establecer un plan riguroso de migración a software libre para todos los
    servicios de información del ayuntamiento.

15. Migrar la ofimática interna a software libre, con los correspondientes
    planes de formación.

16. Que con dinero público sólo se desarrollará código público: asegurar en los nuevos contratos que se publique el código bajo licencias libres.
Diseñar las integraciones con componentes privativos para que sean temporales y fácilmente reemplazables.

17. Implantar los servicios esenciales en ordenadores a los que los
    servidores públicos tengan acceso como mínimo Internet.

18. No comprar hardware que no sea completamente funcional bajo núcleos
    libres, como por ejemplo Linux.

19. Migrar progresivamente a software libre todas las aplicaciones de
    escritorio y el sistema operativo de los ordenadores municipales.

##### Cooperación entre administraciones y con las comunidades de software libre

20. Crear mecanismos de coordinación entre administraciones que permitan
    mancomunar el desarrollo y despliegue de servicios.

21. Priorizar la participación en proyectos ya existentes, procurando que
    las mejoras realizadas se incorporen en el producto original. Respetar
    las prácticas y códigos de conducta de las comunidades en las que se
    participa.

22. Favorecer la reutilización de soluciones mediante la correcta
    documentación y publicidad de los proyectos.


### Política de datos

Que el conocimiento es poder es algo que hemos sabido siempre.
Hoy en día, utilizando datos tenemos mucha posibilidades de conocimiento.
Habrá que encontrar maneras de hacer que los datos estén protegidos, respetando la privacidad de los ciudadanos y ciudadanas.
Por esta razón, es preciso garantizar el acceso público a los datos abiertos así como poner herramientas para evitar monopolios privados de datos.

#### Objetivo general del ámbito

* Hacer una administración transparente y próxima a la ciudadanía.

* Facilitar el acceso y la explotación de los conjuntos de datos públicos.

* Aumentar las colecciones de datos de libre acceso.

* Garantizar, con criterios éticos, la privacidad de los datos personales,
  administrándolos de manera más consciente y reduciendo los riesgos
  derivados de su explotación.


#### Propuesta programática

##### Datos abiertos por defecto

23. Publicar bajo licencias libres y formatos reutilizables toda la
    información pública creada o gestionada por la administración local
    (desde presupuestos municipales hasta la temporización de los semáforos,
    p.e.).

24. Estandarizar los formatos de los datos publicados entre diferentes
    ayuntamientos y administraciones, facilitando así el análisis cruzado de
    los datos.

25. Publicar los datos brutos generados para la realización de estudios
    financiados con dinero público, para que se puedan comprobar los
    estudios o hacer derivados.

26. Promover la reutilización de estos datos, a través de una publicidad
    activa, una buena accesibilidad a la web municipal, y la especificación
    de las licencias.

##### Privacidad de los datos de carácter personal

27. Administrar los datos personales únicamente dentro de la administración
    y siguiendo estrictos criterios de seguridad, garantizando los medios
    técnicos y los conocimientos para gestionarlos sin depender de terceros.

28. Limitar la recogida de datos de carácter personal, en aquellos casos en
    los que previamente se han establecido las finalidades concretas para
    las que serán utilizadas, minimizando así la cantidad de datos
    personales que se recopilan.

29. Implementar mecanismos para dar un máximo de servicios para conocer,
    corregir o borrar datos personales de los sistemas de la administración;
    y no sólo el mínimo legal que exige el RGPD.


### Democratización de la tecnología

El fácil acceso de la ciudadanía a la tecnología, especialmente la relativa a la conectividad a Internet y el uso de teléfonos inteligentes, comporta una transformación de nuestra sociedad hacia una nueva cultura digital.
La democratización de la tecnología, en términos de abaratamiento y facilidad de conectividad, permite la creación de nuevos servicios desde la administración particularizados a las necesidades de sus ciudadanía y accesible desde el teléfono sin necesidades presenciales, ni papeleo.
Esta cultura digital debe impregnar la generación de servicios a la ciudadanía, ya que significan un abaratamiento de los costes de prestación y facilitan el acceso.

La administración debe velar por la igualdad de acceso a estos nuevos servicios, facilitando herramientas y recursos que garanticen su uso y aprofitamiento independientemente del contexto socioeconómico, género o capacidades diversas de la ciudadanía.


#### Objetivo general del ámbito

* Poner a disposición de todos las innovaciones tecnológicas que la
  administración incorpora en su acción municipal, velando por un acceso no
  discriminador a trámites, servicios y acciones digitales de la
  administración.

* Trabajar para disminuir la distancia creada a partir de diferentes usos tecnológicos para parte de los diferentes sectores de la sociedad (brecha digital), creada tanto por el acceso a la tecnología como por conocimiento.
Acompañar todas las acciones municipales para que incluyan una vertiente digital de la formación necesaria.

* Promover políticas divulgativas y formativas que fomenten la
  visibilización de la diversidad de género en el mundo de la tecnología,
  así como el fomento de vocaciones tecnológicas en niñas y chicas.

* Promover una relación consciente y crítica de la ciudadanía con la
  tecnología.

#### Propuesta programática

30. Poner en marcha programas de formación y capacitación digital para la
    ciudadanía, así como fortalecer los existentes.

31. Promover espacios de conexión y uso tecnológico que ofrezcan
    herramientas y recursos de forma gratuita, además de dotar de recursos a
    los ya existentes (bibliotecas, centros cívicos o telecentros).

32. Ofrecer programas pedagógicos en coordinación con centros educativos de
    diferentes niveles que permitan abordar cómo trabajar la tecnología de
    manera inclusiva y respetuosa con la ciudadanía, introduciendo la
    perspectiva de género.

33. Ofrecer formación tecnológica (inicial y avanzada) a colectivos en
    riesgo de exclusión social por motivos socioeconómicos, en temáticas de
    innovación tecnológica, en coordinación con los servicios sociales
    municipales.

34. Garantizar que el personal de la administración podrá acompañar a la
    ciudadanía en su interacción digital con la administración a partir de
    programas de formación interna.

35. Garantizar la accesibilidad a las personas con discapacidades.
Es necesario que los formatos de representación de la información cumplan con los estándares y recomendaciones respecto a la representación de la información para personas con capacidades diversas.

36. Hay que garantizar que los modelos y algoritmos, especialmente en el
    contexto del aprendizaje automático, no introduzcan sesgo que perjudique
    a las personas, como por ejemplo sexo, raza, religión u orientación
    sexual.


### Compra pública de dispositivos electrónicos y circularidad

En el momento de determinar las condiciones de compra publica de un dispositivo.

#### Objetivo general del ámbito

* Compra de dispositivos electrónicos que minimice el impacto negativo en
  las personas y el medio ambiente.

#### Propuesta programática

37. Promoción de la reducción del impacto medioambiental y de la economía
    social y solidaria.

38. Respeto a los derechos laborales de las personas trabajadoras
    involucradas en la fabricación, mantenimiento, recogida y reciclaje de
    dispositivos electrónicos.

39. Compra pública responsable que respete los derechos laborales en la
    producción de bienes electrónicos.

40. Trazabilidad en origen de los dispositivos para asegurar la
    circularidad: reparación, renovación, reutilización, garantizar el
    reciclaje final y la transparencia de los datos sobre estos procesos.

41. Compromiso de donación de los dispositivos a entidades sociales al final
    de su uso, para alargar la vida útil de los dispositivos y crear
    beneficios sociales con la reutilización.

42. Compra pública con extensión de garantía que incluya reparación y
    mantenimiento durante todo el período de utilización, para facilitar la
    extensión de la vida útil de los dispositivos.

43. Fomento del conocimiento sobre mantenimiento y la reparación de los
    dispositivos en las entidades públicas y los centros educativos
    (colegios, universidades).


### Estándares libres

Los documentos, datos e interacción entre servicios municipales y personas son una necesidad y un recurso para la ciudadanía en general para muchos aspectos de su vida.
El formato y las herramientas de acceso no deben ser nunca un obstáculo.
Las especificaciones y formatos libres están completamente documentados y están disponibles públicamente; deben estar libres de restricciones de derechos de autor, reclamaciones de propiedad intelectual o licencias restrictivas; el formato se decidide por una organización de estándares independientes de un proveedor o por una comunidad (por ejemplo, comunidad de desarrollo de código abierto); hay implementaciones de herramientas de soporte de uso libre.

#### Objetivos generales

* Facilitar la interoperabilidad y compartición de datos en los servicios
  financiados con fondos públicos en beneficio de los ciudadanos, evitando
  barreras de acceso, utilizando especificaciones para los formatos de datos
  y servicios que sigan estándares abiertos.

* Evitar crear dependencias y costes de terceros: preferencia por estándares
  de formatos de datos y protocolos libres de patentes, licencias, derechos
  intelectuales.

* Evitar condicionar decisiones con la preferencia por protocolos libres,
  ampliamente implementados y aplicados en una variedad de productos y
  servicios.

#### Propuesta programática

44. Garantizar la interoperabilidad: utilizar formatos abiertos de archivos como una herramienta para garantizar el acceso a la información sin obstáculos y la buena interacción con y entre los sistemas públicos.
Además, los estándares que se utilicen deben tener implementaciones abiertas que se verifique funcionen con una variedad de dispositivos o software, incluyendo siempre una opción de software libre.

45. Uno de los requisitos en el momento de comprar aparatos tecnológicos ha de ser que estos utilicen estándares libres para realizar su función.
De esta manera se eliminan las dependencias de un único fabricante.

46. Producción digital utilizando licencias copyleft y Creative Commons que
    facilitan la reutilización de obras artísticas y producciones de los
    creadores locales financiadas públicamente, como herramientas de cultura
    libre.

47. Establecer políticas activas de uso de estándares abiertos y migración a
    software libre, habiendo establecido una lista de aplicaciones
    homologadas para cada uso y funcionalidad requerida para el sector
    público, y habilitar estragegias de reutilización, colaboración y
    compartición de esfuerzos con otras entidades públicas del mundo.
